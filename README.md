# OpenAPIClient-php

Welcome to the official DHL Express APIs (MyDHL API) below are the published API Documentation to fulfill your shipping needs with DHL Express. 
    
Please follow the process described [here](https://developer.dhl.com/api-reference/dhl-express-mydhl-api#get-started-section/user-guide%get-access) to request access to the DHL Express - MyDHL API services  

In case you already have DHL Express - MyDHL API Service credentials please ensure to use the endpoints/environments listed 
[here](https://developer.dhl.com/api-reference/dhl-express-mydhl-api#get-started-section/user-guide%environments)


For more information, please visit [https://developer.dhl.com/express](https://developer.dhl.com/express).

## Installation & Usage

### Requirements

PHP 7.3 and later.
Should also work with PHP 8.0 but has not been tested.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure HTTP basic authorization: basicAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new OpenAPI\Client\Api\AddressApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$type = pickup; // string
$country_code = CZ; // string | A short text string code (see values defined in ISO 3166) specifying the shipment origin country. https://gs1.org/voc/Country, Alpha-2 Code
$postal_code = 14800; // string | Text specifying the postal code for an address. https://gs1.org/voc/postalCode
$city_name = Prague; // string | Text specifying the city name
$strict_validation = true; // bool | If set to true service will return no records when exact valid match not found
$message_reference = d0e7832e-5c98-11ea-bc55-0242ac13; // string | Please provide message reference
$message_reference_date = Wed, 21 Oct 2015 07:28:00 GMT; // string | Optional reference date in the  HTTP-date format https://tools.ietf.org/html/rfc7231#section-7.1.1.2
$plugin_name =  ; // string | Please provide name of the plugin (applicable to 3PV only)
$plugin_version =  ; // string | Please provide version of the plugin (applicable to 3PV only)
$shipping_system_platform_name =  ; // string | Please provide name of the shipping platform(applicable to 3PV only)
$shipping_system_platform_version =  ; // string | Please provide version of the shipping platform (applicable to 3PV only)
$webstore_platform_name =  ; // string | Please provide name of the webstore platform (applicable to 3PV only)
$webstore_platform_version =  ; // string | Please provide version of the webstore platform (applicable to 3PV only)

try {
    $result = $apiInstance->expApiAddressValidate($type, $country_code, $postal_code, $city_name, $strict_validation, $message_reference, $message_reference_date, $plugin_name, $plugin_version, $shipping_system_platform_name, $shipping_system_platform_version, $webstore_platform_name, $webstore_platform_version);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddressApi->expApiAddressValidate: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://api-mock.dhl.com/mydhlapi*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AddressApi* | [**expApiAddressValidate**](docs/Api/AddressApi.md#expapiaddressvalidate) | **GET** /address-validate | Validate DHL Express pickup/delivery capabilities at origin/destination
*IdentifierApi* | [**expApiIdentifiers**](docs/Api/IdentifierApi.md#expapiidentifiers) | **GET** /identifiers | Service to allocate identifiers upfront for DHL Express Breakbulk or Loose Break Bulk shipments
*InvoiceApi* | [**expApiShipmentsInvoiceData**](docs/Api/InvoiceApi.md#expapishipmentsinvoicedata) | **POST** /invoices/upload-invoice-data | Upload Commercial invoice data
*PickupApi* | [**expApiPickups**](docs/Api/PickupApi.md#expapipickups) | **POST** /pickups | Create a DHL Express pickup booking request
*PickupApi* | [**expApiPickupsCancel**](docs/Api/PickupApi.md#expapipickupscancel) | **DELETE** /pickups/{dispatchConfirmationNumber} | Cancel a DHL Express pickup booking request
*PickupApi* | [**expApiPickupsUpdate**](docs/Api/PickupApi.md#expapipickupsupdate) | **PATCH** /pickups/{dispatchConfirmationNumber} | Update pickup information for an existing DHL Express pickup booking request
*ProductApi* | [**expApiProducts**](docs/Api/ProductApi.md#expapiproducts) | **GET** /products | Retrieve available DHL Express products for a one piece Shipment
*RatingApi* | [**expApiLandedCost**](docs/Api/RatingApi.md#expapilandedcost) | **POST** /landed-cost | Landed Cost
*RatingApi* | [**expApiRates**](docs/Api/RatingApi.md#expapirates) | **GET** /rates | Retrieve Rates for a one piece Shipment
*RatingApi* | [**expApiRatesMany**](docs/Api/RatingApi.md#expapiratesmany) | **POST** /rates | Retrieve Rates for Multi-piece Shipments
*ShipmentApi* | [**expApiShipments**](docs/Api/ShipmentApi.md#expapishipments) | **POST** /shipments | Create Shipment
*ShipmentApi* | [**expApiShipmentsEpod**](docs/Api/ShipmentApi.md#expapishipmentsepod) | **GET** /shipments/{shipmentTrackingNumber}/proof-of-delivery | Electronic Proof of Delivery
*ShipmentApi* | [**expApiShipmentsImgUpload**](docs/Api/ShipmentApi.md#expapishipmentsimgupload) | **PATCH** /shipments/{shipmentTrackingNumber}/upload-image | Upload updated customs documentation for your DHL Express shipment
*ShipmentApi* | [**expApiShipmentsInvoiceDataAwb**](docs/Api/ShipmentApi.md#expapishipmentsinvoicedataawb) | **PATCH** /shipments/{shipmentTrackingNumber}/upload-invoice-data | Upload Commercial Invoice data for your DHL Express shipment
*TrackingApi* | [**expApiShipmentsTracking**](docs/Api/TrackingApi.md#expapishipmentstracking) | **GET** /shipments/{shipmentTrackingNumber}/tracking | Track a single DHL Express Shipment
*TrackingApi* | [**expApiShipmentsTrackingMulti**](docs/Api/TrackingApi.md#expapishipmentstrackingmulti) | **GET** /tracking | Track a single or multiple DHL Express Shipments

## Models

- [Dimensions](docs/Model/Dimensions.md)
- [Dimensions1](docs/Model/Dimensions1.md)
- [Dimensions2](docs/Model/Dimensions2.md)
- [Pickup](docs/Model/Pickup.md)
- [PickupPickupDetails](docs/Model/PickupPickupDetails.md)
- [PickupPickupRequestorDetails](docs/Model/PickupPickupRequestorDetails.md)
- [PickupSpecialInstructions](docs/Model/PickupSpecialInstructions.md)
- [SupermodelIoLogisticsExpressAccount](docs/Model/SupermodelIoLogisticsExpressAccount.md)
- [SupermodelIoLogisticsExpressAddress](docs/Model/SupermodelIoLogisticsExpressAddress.md)
- [SupermodelIoLogisticsExpressAddressCreateShipmentResponse](docs/Model/SupermodelIoLogisticsExpressAddressCreateShipmentResponse.md)
- [SupermodelIoLogisticsExpressAddressRatesRequest](docs/Model/SupermodelIoLogisticsExpressAddressRatesRequest.md)
- [SupermodelIoLogisticsExpressAddressValidateResponse](docs/Model/SupermodelIoLogisticsExpressAddressValidateResponse.md)
- [SupermodelIoLogisticsExpressAddressValidateResponseAddress](docs/Model/SupermodelIoLogisticsExpressAddressValidateResponseAddress.md)
- [SupermodelIoLogisticsExpressAddressValidateResponseServiceArea](docs/Model/SupermodelIoLogisticsExpressAddressValidateResponseServiceArea.md)
- [SupermodelIoLogisticsExpressContact](docs/Model/SupermodelIoLogisticsExpressContact.md)
- [SupermodelIoLogisticsExpressContactBuyer](docs/Model/SupermodelIoLogisticsExpressContactBuyer.md)
- [SupermodelIoLogisticsExpressContactCreateShipmentResponse](docs/Model/SupermodelIoLogisticsExpressContactCreateShipmentResponse.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequest](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequest.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContent](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContent.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclaration](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclaration.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationAdditionalCharges](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationAdditionalCharges.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCommodityCodes](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCommodityCodes.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomerReferences](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomerReferences.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomsDocuments](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomsDocuments.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomsDocuments1](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomsDocuments1.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationDeclarationNotes](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationDeclarationNotes.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationExporter](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationExporter.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationInvoice](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationInvoice.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationInvoiceCustomerReferences](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationInvoiceCustomerReferences.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationLicenses](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationLicenses.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationLineItems](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationLineItems.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationQuantity](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationQuantity.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationRemarks](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationRemarks.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationWeight](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationWeight.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetails](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetails.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsBuyerDetails](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsBuyerDetails.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsExporterDetails](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsExporterDetails.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsImporterDetails](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsImporterDetails.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsPayerDetails](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsPayerDetails.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsReceiverDetails](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsReceiverDetails.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsSellerDetails](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsSellerDetails.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsShipperDetails](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsShipperDetails.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestOutputImageProperties](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestOutputImageProperties.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestOutputImagePropertiesCustomerBarcodes](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestOutputImagePropertiesCustomerBarcodes.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestOutputImagePropertiesCustomerLogos](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestOutputImagePropertiesCustomerLogos.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestParentShipment](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestParentShipment.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestPrepaidCharges](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestPrepaidCharges.md)
- [SupermodelIoLogisticsExpressCreateShipmentRequestShipmentNotification](docs/Model/SupermodelIoLogisticsExpressCreateShipmentRequestShipmentNotification.md)
- [SupermodelIoLogisticsExpressCreateShipmentResponse](docs/Model/SupermodelIoLogisticsExpressCreateShipmentResponse.md)
- [SupermodelIoLogisticsExpressCreateShipmentResponseCustomerDetails](docs/Model/SupermodelIoLogisticsExpressCreateShipmentResponseCustomerDetails.md)
- [SupermodelIoLogisticsExpressCreateShipmentResponseCustomerDetailsShipperDetails](docs/Model/SupermodelIoLogisticsExpressCreateShipmentResponseCustomerDetailsShipperDetails.md)
- [SupermodelIoLogisticsExpressCreateShipmentResponseDocuments](docs/Model/SupermodelIoLogisticsExpressCreateShipmentResponseDocuments.md)
- [SupermodelIoLogisticsExpressCreateShipmentResponseDocuments1](docs/Model/SupermodelIoLogisticsExpressCreateShipmentResponseDocuments1.md)
- [SupermodelIoLogisticsExpressCreateShipmentResponsePackages](docs/Model/SupermodelIoLogisticsExpressCreateShipmentResponsePackages.md)
- [SupermodelIoLogisticsExpressCreateShipmentResponseServiceBreakdown](docs/Model/SupermodelIoLogisticsExpressCreateShipmentResponseServiceBreakdown.md)
- [SupermodelIoLogisticsExpressCreateShipmentResponseShipmentCharges](docs/Model/SupermodelIoLogisticsExpressCreateShipmentResponseShipmentCharges.md)
- [SupermodelIoLogisticsExpressCreateShipmentResponseShipmentDetails](docs/Model/SupermodelIoLogisticsExpressCreateShipmentResponseShipmentDetails.md)
- [SupermodelIoLogisticsExpressEPODResponse](docs/Model/SupermodelIoLogisticsExpressEPODResponse.md)
- [SupermodelIoLogisticsExpressEPODResponseDocuments](docs/Model/SupermodelIoLogisticsExpressEPODResponseDocuments.md)
- [SupermodelIoLogisticsExpressExportDeclaration](docs/Model/SupermodelIoLogisticsExpressExportDeclaration.md)
- [SupermodelIoLogisticsExpressExportDeclarationAdditionalCharges](docs/Model/SupermodelIoLogisticsExpressExportDeclarationAdditionalCharges.md)
- [SupermodelIoLogisticsExpressExportDeclarationCustomerReferences](docs/Model/SupermodelIoLogisticsExpressExportDeclarationCustomerReferences.md)
- [SupermodelIoLogisticsExpressExportDeclarationCustomsDocuments](docs/Model/SupermodelIoLogisticsExpressExportDeclarationCustomsDocuments.md)
- [SupermodelIoLogisticsExpressExportDeclarationCustomsDocuments1](docs/Model/SupermodelIoLogisticsExpressExportDeclarationCustomsDocuments1.md)
- [SupermodelIoLogisticsExpressExportDeclarationInvoice](docs/Model/SupermodelIoLogisticsExpressExportDeclarationInvoice.md)
- [SupermodelIoLogisticsExpressExportDeclarationInvoiceCustomerReferences](docs/Model/SupermodelIoLogisticsExpressExportDeclarationInvoiceCustomerReferences.md)
- [SupermodelIoLogisticsExpressExportDeclarationLineItems](docs/Model/SupermodelIoLogisticsExpressExportDeclarationLineItems.md)
- [SupermodelIoLogisticsExpressExportDeclarationQuantity](docs/Model/SupermodelIoLogisticsExpressExportDeclarationQuantity.md)
- [SupermodelIoLogisticsExpressExportDeclarationRemarks](docs/Model/SupermodelIoLogisticsExpressExportDeclarationRemarks.md)
- [SupermodelIoLogisticsExpressIdentifier](docs/Model/SupermodelIoLogisticsExpressIdentifier.md)
- [SupermodelIoLogisticsExpressIdentifierResponse](docs/Model/SupermodelIoLogisticsExpressIdentifierResponse.md)
- [SupermodelIoLogisticsExpressIdentifierResponseIdentifiers](docs/Model/SupermodelIoLogisticsExpressIdentifierResponseIdentifiers.md)
- [SupermodelIoLogisticsExpressImageUploadRequest](docs/Model/SupermodelIoLogisticsExpressImageUploadRequest.md)
- [SupermodelIoLogisticsExpressLandedCostRequest](docs/Model/SupermodelIoLogisticsExpressLandedCostRequest.md)
- [SupermodelIoLogisticsExpressLandedCostRequestAdditionalQuantityDefinitions](docs/Model/SupermodelIoLogisticsExpressLandedCostRequestAdditionalQuantityDefinitions.md)
- [SupermodelIoLogisticsExpressLandedCostRequestCharges](docs/Model/SupermodelIoLogisticsExpressLandedCostRequestCharges.md)
- [SupermodelIoLogisticsExpressLandedCostRequestCustomerDetails](docs/Model/SupermodelIoLogisticsExpressLandedCostRequestCustomerDetails.md)
- [SupermodelIoLogisticsExpressLandedCostRequestGoodsCharacteristics](docs/Model/SupermodelIoLogisticsExpressLandedCostRequestGoodsCharacteristics.md)
- [SupermodelIoLogisticsExpressLandedCostRequestItems](docs/Model/SupermodelIoLogisticsExpressLandedCostRequestItems.md)
- [SupermodelIoLogisticsExpressPackage](docs/Model/SupermodelIoLogisticsExpressPackage.md)
- [SupermodelIoLogisticsExpressPackageLabelBarcodes](docs/Model/SupermodelIoLogisticsExpressPackageLabelBarcodes.md)
- [SupermodelIoLogisticsExpressPackageLabelText](docs/Model/SupermodelIoLogisticsExpressPackageLabelText.md)
- [SupermodelIoLogisticsExpressPackageRR](docs/Model/SupermodelIoLogisticsExpressPackageRR.md)
- [SupermodelIoLogisticsExpressPackageReference](docs/Model/SupermodelIoLogisticsExpressPackageReference.md)
- [SupermodelIoLogisticsExpressPickupRequest](docs/Model/SupermodelIoLogisticsExpressPickupRequest.md)
- [SupermodelIoLogisticsExpressPickupRequestCustomerDetails](docs/Model/SupermodelIoLogisticsExpressPickupRequestCustomerDetails.md)
- [SupermodelIoLogisticsExpressPickupRequestCustomerDetailsBookingRequestorDetails](docs/Model/SupermodelIoLogisticsExpressPickupRequestCustomerDetailsBookingRequestorDetails.md)
- [SupermodelIoLogisticsExpressPickupRequestCustomerDetailsShipperDetails](docs/Model/SupermodelIoLogisticsExpressPickupRequestCustomerDetailsShipperDetails.md)
- [SupermodelIoLogisticsExpressPickupRequestShipmentDetails](docs/Model/SupermodelIoLogisticsExpressPickupRequestShipmentDetails.md)
- [SupermodelIoLogisticsExpressPickupRequestSpecialInstructions](docs/Model/SupermodelIoLogisticsExpressPickupRequestSpecialInstructions.md)
- [SupermodelIoLogisticsExpressPickupResponse](docs/Model/SupermodelIoLogisticsExpressPickupResponse.md)
- [SupermodelIoLogisticsExpressProducts](docs/Model/SupermodelIoLogisticsExpressProducts.md)
- [SupermodelIoLogisticsExpressProductsBreakdown](docs/Model/SupermodelIoLogisticsExpressProductsBreakdown.md)
- [SupermodelIoLogisticsExpressProductsDeliveryCapabilities](docs/Model/SupermodelIoLogisticsExpressProductsDeliveryCapabilities.md)
- [SupermodelIoLogisticsExpressProductsPickupCapabilities](docs/Model/SupermodelIoLogisticsExpressProductsPickupCapabilities.md)
- [SupermodelIoLogisticsExpressProductsProducts](docs/Model/SupermodelIoLogisticsExpressProductsProducts.md)
- [SupermodelIoLogisticsExpressRateRequest](docs/Model/SupermodelIoLogisticsExpressRateRequest.md)
- [SupermodelIoLogisticsExpressRateRequestCustomerDetails](docs/Model/SupermodelIoLogisticsExpressRateRequestCustomerDetails.md)
- [SupermodelIoLogisticsExpressRateRequestMonetaryAmount](docs/Model/SupermodelIoLogisticsExpressRateRequestMonetaryAmount.md)
- [SupermodelIoLogisticsExpressRateRequestProductsAndServices](docs/Model/SupermodelIoLogisticsExpressRateRequestProductsAndServices.md)
- [SupermodelIoLogisticsExpressRates](docs/Model/SupermodelIoLogisticsExpressRates.md)
- [SupermodelIoLogisticsExpressRatesBreakdown](docs/Model/SupermodelIoLogisticsExpressRatesBreakdown.md)
- [SupermodelIoLogisticsExpressRatesBreakdown1](docs/Model/SupermodelIoLogisticsExpressRatesBreakdown1.md)
- [SupermodelIoLogisticsExpressRatesDetailedPriceBreakdown](docs/Model/SupermodelIoLogisticsExpressRatesDetailedPriceBreakdown.md)
- [SupermodelIoLogisticsExpressRatesExchangeRates](docs/Model/SupermodelIoLogisticsExpressRatesExchangeRates.md)
- [SupermodelIoLogisticsExpressRatesItems](docs/Model/SupermodelIoLogisticsExpressRatesItems.md)
- [SupermodelIoLogisticsExpressRatesPickupCapabilities](docs/Model/SupermodelIoLogisticsExpressRatesPickupCapabilities.md)
- [SupermodelIoLogisticsExpressRatesPriceBreakdown](docs/Model/SupermodelIoLogisticsExpressRatesPriceBreakdown.md)
- [SupermodelIoLogisticsExpressRatesPriceBreakdown1](docs/Model/SupermodelIoLogisticsExpressRatesPriceBreakdown1.md)
- [SupermodelIoLogisticsExpressRatesPriceBreakdown2](docs/Model/SupermodelIoLogisticsExpressRatesPriceBreakdown2.md)
- [SupermodelIoLogisticsExpressRatesProducts](docs/Model/SupermodelIoLogisticsExpressRatesProducts.md)
- [SupermodelIoLogisticsExpressRatesTotalPrice](docs/Model/SupermodelIoLogisticsExpressRatesTotalPrice.md)
- [SupermodelIoLogisticsExpressRatesTotalPriceBreakdown](docs/Model/SupermodelIoLogisticsExpressRatesTotalPriceBreakdown.md)
- [SupermodelIoLogisticsExpressReference](docs/Model/SupermodelIoLogisticsExpressReference.md)
- [SupermodelIoLogisticsExpressRegistrationNumbers](docs/Model/SupermodelIoLogisticsExpressRegistrationNumbers.md)
- [SupermodelIoLogisticsExpressTrackingResponse](docs/Model/SupermodelIoLogisticsExpressTrackingResponse.md)
- [SupermodelIoLogisticsExpressTrackingResponseEvents](docs/Model/SupermodelIoLogisticsExpressTrackingResponseEvents.md)
- [SupermodelIoLogisticsExpressTrackingResponseEvents1](docs/Model/SupermodelIoLogisticsExpressTrackingResponseEvents1.md)
- [SupermodelIoLogisticsExpressTrackingResponsePieces](docs/Model/SupermodelIoLogisticsExpressTrackingResponsePieces.md)
- [SupermodelIoLogisticsExpressTrackingResponseReceiverDetails](docs/Model/SupermodelIoLogisticsExpressTrackingResponseReceiverDetails.md)
- [SupermodelIoLogisticsExpressTrackingResponseReceiverDetailsPostalAddress](docs/Model/SupermodelIoLogisticsExpressTrackingResponseReceiverDetailsPostalAddress.md)
- [SupermodelIoLogisticsExpressTrackingResponseReceiverDetailsServiceArea](docs/Model/SupermodelIoLogisticsExpressTrackingResponseReceiverDetailsServiceArea.md)
- [SupermodelIoLogisticsExpressTrackingResponseServiceArea](docs/Model/SupermodelIoLogisticsExpressTrackingResponseServiceArea.md)
- [SupermodelIoLogisticsExpressTrackingResponseServiceArea1](docs/Model/SupermodelIoLogisticsExpressTrackingResponseServiceArea1.md)
- [SupermodelIoLogisticsExpressTrackingResponseShipments](docs/Model/SupermodelIoLogisticsExpressTrackingResponseShipments.md)
- [SupermodelIoLogisticsExpressTrackingResponseShipperDetails](docs/Model/SupermodelIoLogisticsExpressTrackingResponseShipperDetails.md)
- [SupermodelIoLogisticsExpressTrackingResponseShipperDetailsPostalAddress](docs/Model/SupermodelIoLogisticsExpressTrackingResponseShipperDetailsPostalAddress.md)
- [SupermodelIoLogisticsExpressTrackingResponseShipperDetailsServiceArea](docs/Model/SupermodelIoLogisticsExpressTrackingResponseShipperDetailsServiceArea.md)
- [SupermodelIoLogisticsExpressUpdatePickupRequest](docs/Model/SupermodelIoLogisticsExpressUpdatePickupRequest.md)
- [SupermodelIoLogisticsExpressUpdatePickupRequestShipmentDetails](docs/Model/SupermodelIoLogisticsExpressUpdatePickupRequestShipmentDetails.md)
- [SupermodelIoLogisticsExpressUpdatePickupResponse](docs/Model/SupermodelIoLogisticsExpressUpdatePickupResponse.md)
- [SupermodelIoLogisticsExpressUploadInvoiceDataRequest](docs/Model/SupermodelIoLogisticsExpressUploadInvoiceDataRequest.md)
- [SupermodelIoLogisticsExpressUploadInvoiceDataRequestContent](docs/Model/SupermodelIoLogisticsExpressUploadInvoiceDataRequestContent.md)
- [SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetails](docs/Model/SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetails.md)
- [SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsBuyerDetails](docs/Model/SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsBuyerDetails.md)
- [SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsExporterDetails](docs/Model/SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsExporterDetails.md)
- [SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsImporterDetails](docs/Model/SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsImporterDetails.md)
- [SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsSellerDetails](docs/Model/SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsSellerDetails.md)
- [SupermodelIoLogisticsExpressUploadInvoiceDataRequestOutputImageProperties](docs/Model/SupermodelIoLogisticsExpressUploadInvoiceDataRequestOutputImageProperties.md)
- [SupermodelIoLogisticsExpressUploadInvoiceDataRequestOutputImagePropertiesImageOptions](docs/Model/SupermodelIoLogisticsExpressUploadInvoiceDataRequestOutputImagePropertiesImageOptions.md)
- [SupermodelIoLogisticsExpressUploadInvoiceDataRequestSID](docs/Model/SupermodelIoLogisticsExpressUploadInvoiceDataRequestSID.md)
- [SupermodelIoLogisticsExpressUploadInvoiceDataResponse](docs/Model/SupermodelIoLogisticsExpressUploadInvoiceDataResponse.md)
- [SupermodelIoLogisticsExpressValueAddedServices](docs/Model/SupermodelIoLogisticsExpressValueAddedServices.md)
- [SupermodelIoLogisticsExpressValueAddedServicesRates](docs/Model/SupermodelIoLogisticsExpressValueAddedServicesRates.md)
- [Weight](docs/Model/Weight.md)
- [Weight1](docs/Model/Weight1.md)

## Authorization

### basicAuth

- **Type**: HTTP basic authentication

## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author

support@dpdhl.freshdesk.com

## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `2.3.0`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
