#!/bin/bash

if [ ! -f openapi-generator-cli.jar ]; then
    echo openapi-generator-cli.jar missing, installing..
    wget https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/5.3.0/openapi-generator-cli-5.3.0.jar -O openapi-generator-cli.jar
fi

java -jar openapi-generator-cli.jar generate -i https://developer.dhl.com/sites/default/files/2021-10/dpdhl-express-api-2.3.0-swagger.yaml -g php -o .

echo === Patching composer.json

jq -s add composer.json composer.add.json > composer.json.temp
rm composer.json
mv composer.json.temp composer.json

echo === Patching .gitignore
cat .gitignore .gitignore.extra >> .gitignore.tmp
rm .gitignore
mv .gitignore.tmp .gitignore

# not needed. Parameter kann auch als String übergeben werden, dann alles gut.
#echo == Patching ProductApi
#search="is_customs_declarable;"
#replace="is_customs_declarable \? 'true' : 'false';"
#sed -i "s/$search/$replace/" lib/Api/ProductApi.php

