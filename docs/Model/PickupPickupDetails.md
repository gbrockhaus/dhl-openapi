# # PickupPickupDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**postal_address** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressAddress**](SupermodelIoLogisticsExpressAddress.md) |  |
**contact_information** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressContact**](SupermodelIoLogisticsExpressContact.md) |  |
**registration_numbers** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRegistrationNumbers[]**](SupermodelIoLogisticsExpressRegistrationNumbers.md) |  | [optional]
**bank_details** | **object[]** |  | [optional]
**type_code** | **string** | Please enter the business party type related to the pickup | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
