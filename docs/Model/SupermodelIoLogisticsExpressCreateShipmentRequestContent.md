# # SupermodelIoLogisticsExpressCreateShipmentRequestContent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**packages** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressPackage[]**](SupermodelIoLogisticsExpressPackage.md) | Here you can define properties per package |
**is_customs_declarable** | **bool** | For customs purposes please advise if your shipment is dutiable (true) or non dutiable (false) |
**declared_value** | **float** | For customs purposes please advise on declared value of the shipment | [optional]
**declared_value_currency** | **string** | For customs purposes please advise on declared value currency code of the shipment | [optional]
**export_declaration** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclaration**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclaration.md) |  | [optional]
**description** | **string** | Please enter description of your shipment |
**us_filing_type_value** | **string** | This is used for the US AES4, FTR and ITN numbers to be printed on the Transport Label | [optional]
**incoterm** | **string** | The Incoterms rules are a globally-recognized set of standards, used worldwide in international and domestic contracts for the delivery of goods, illustrating responsibilities between buyer and seller for costs and risk, as well as cargo insurance.&lt;br&gt;EXW ExWorks&lt;br&gt;FCA Free Carrier&lt;br&gt;CPT Carriage Paid To&lt;br&gt;CIP Carriage and Insurance Paid To&lt;br&gt;DPU Delivered at Place Unloaded&lt;br&gt;DAP Delivered at Place&lt;br&gt;DDP Delivered Duty Paid&lt;br&gt;FAS Free Alongside Ship&lt;br&gt;FOB Free on Board&lt;br&gt;CFR Cost and Freight&lt;br&gt;CIF Cost, Insurance and Freight&lt;br&gt;&lt;br&gt;&lt;br&gt; |
**unit_of_measurement** | **string** | Please enter Unit of measurement - metric,imperial |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
