# # SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclaration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_items** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationLineItems[]**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationLineItems.md) | Please enter details for each export line item |
**invoice** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationInvoice**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationInvoice.md) |  | [optional]
**remarks** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationRemarks[]**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationRemarks.md) | Please enter up to three remarks | [optional]
**additional_charges** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationAdditionalCharges[]**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationAdditionalCharges.md) | Please enter additional charge to appear on the invoice&lt;br&gt;admin, Administration Charge&lt;br&gt;delivery, Delivery Charge&lt;br&gt;documentation, Documentation Charge&lt;br&gt;expedite, Expedite Charge&lt;br&gt;freight, Freight Charge&lt;br&gt;fuel surcharge, Fuel Surcharge&lt;br&gt;logistic, Logistic Charge&lt;br&gt;other, Other Charge&lt;br&gt;packaging, Packaging Charge&lt;br&gt;pickup, Pickup Charge&lt;br&gt;handling, Handling Charge&lt;br&gt;vat, VAT Charge&lt;br&gt;insurance, Insurance Cost | [optional]
**destination_port_name** | **string** | Please provide destination port details | [optional]
**place_of_incoterm** | **string** | Name of port of departure, shipment or destination as required under the applicable delivery term. | [optional]
**payer_vat_number** | **string** | Please provide Payer VAT number | [optional]
**recipient_reference** | **string** | Please enter recipient reference | [optional]
**exporter** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationExporter**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationExporter.md) |  | [optional]
**package_marks** | **string** | Please enter package marks | [optional]
**declaration_notes** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationDeclarationNotes[]**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationDeclarationNotes.md) | Please provide up to three dcelaration notes | [optional]
**export_reference** | **string** | Please enter export reference | [optional]
**export_reason** | **string** | Please enter export reason | [optional]
**export_reason_type** | **string** | Please provide the reason for export | [optional]
**licenses** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationLicenses[]**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationLicenses.md) | Please provide details about export and import licenses | [optional]
**shipment_type** | **string** | Please provide the shipment was sent for Personal (Gift) or Commercial (Sale) reasons | [optional]
**customs_documents** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomsDocuments1[]**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomsDocuments1.md) | Please provide the Customs Documents at invoice level | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
