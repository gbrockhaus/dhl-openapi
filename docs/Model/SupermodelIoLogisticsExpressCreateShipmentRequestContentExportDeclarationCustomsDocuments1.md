# # SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomsDocuments1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type_code** | **string** | Please provide the customs document type code |
**value** | **string** | Please provide the Customs Document ID |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
