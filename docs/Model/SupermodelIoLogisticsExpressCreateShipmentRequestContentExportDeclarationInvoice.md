# # SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationInvoice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **string** | Please enter commercial invoice number |
**date** | **\DateTime** | Please enter commercial invoice date |
**signature_name** | **string** | Please enter who has signed the invoce | [optional]
**signature_title** | **string** | Please provide title of person who has signed the invoice | [optional]
**signature_image** | **string** | Please provide the signature image | [optional]
**instructions** | **string[]** | Please provide the invoice instruction | [optional]
**customer_data_text_entries** | **string[]** | Please provide the Customer data text details | [optional]
**function** | **string** | Please provide the purpose was the document details captured and are planned to be used. | [optional]
**total_net_weight** | **float** | Please provide the total net weight | [optional]
**total_gross_weight** | **float** | Please provide the total gross weight | [optional]
**customer_references** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationInvoiceCustomerReferences[]**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationInvoiceCustomerReferences.md) | Please provide the customer references at invoice level | [optional]
**terms_of_payment** | **string** | Please provide the terms of payment | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
