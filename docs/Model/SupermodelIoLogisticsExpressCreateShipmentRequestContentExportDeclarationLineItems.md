# # SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationLineItems

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **int** | Please provide line item number |
**description** | **string** | Please provide description of the line item |
**price** | **float** | Please provide monetary value of the line item |
**quantity** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationQuantity**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationQuantity.md) |  |
**commodity_codes** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCommodityCodes[]**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCommodityCodes.md) | Please provide Commodity codes for the shipment at item line level | [optional]
**export_reason_type** | **string** | Please provide the reason for export | [optional]
**manufacturer_country** | **string** | Please enter two letter ISO manufacturer country code |
**export_control_classification_number** | **string** | Please enter Export Control Classification Number) info&lt;br&gt;This is required for EEI filing US country usage | [optional]
**weight** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationWeight**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationWeight.md) |  |
**is_taxes_paid** | **bool** | Please provide if the Taxes is paid for the line item | [optional]
**additional_information** | **string[]** | Please provide the additional information | [optional]
**customer_references** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomerReferences[]**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomerReferences.md) | Please provide the Customer References for the line item | [optional]
**customs_documents** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomsDocuments[]**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCustomsDocuments.md) | Please provide the customs documents details | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
