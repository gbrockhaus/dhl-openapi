# # SupermodelIoLogisticsExpressCreateShipmentResponseCustomerDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipper_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentResponseCustomerDetailsShipperDetails**](SupermodelIoLogisticsExpressCreateShipmentResponseCustomerDetailsShipperDetails.md) |  | [optional]
**receiver_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentResponseCustomerDetailsShipperDetails**](SupermodelIoLogisticsExpressCreateShipmentResponseCustomerDetailsShipperDetails.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
