# # SupermodelIoLogisticsExpressCreateShipmentResponseShipmentDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**service_handling_feature_codes** | **string[]** | This array contains all the DHL Express special handling feature codes | [optional]
**volumetric_weight** | **float** | Here you can find calculated volumetric weight based on dimensions provided in the request | [optional]
**billing_code** | **string** | Here you can find billing code which was applied on your shipment | [optional]
**service_content_code** | **string** | Here you can find the DHL Express shipment content code of your shipment | [optional]
**customer_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentResponseCustomerDetails**](SupermodelIoLogisticsExpressCreateShipmentResponseCustomerDetails.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
