# # SupermodelIoLogisticsExpressExportDeclaration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_items** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressExportDeclarationLineItems[]**](SupermodelIoLogisticsExpressExportDeclarationLineItems.md) | Please enter details for each export line item |
**invoice** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressExportDeclarationInvoice**](SupermodelIoLogisticsExpressExportDeclarationInvoice.md) |  |
**remarks** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressExportDeclarationRemarks[]**](SupermodelIoLogisticsExpressExportDeclarationRemarks.md) | Please enter up to three remarks | [optional]
**additional_charges** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressExportDeclarationAdditionalCharges[]**](SupermodelIoLogisticsExpressExportDeclarationAdditionalCharges.md) | Please enter additional charge to appear on the invoice&lt;br&gt;admin, Administration Charge&lt;br&gt;delivery, Delivery Charge&lt;br&gt;documentation, Documentation Charge&lt;br&gt;expedite, Expedite Charge&lt;br&gt;freight, Freight Charge&lt;br&gt;fuel surcharge, Fuel Surcharge&lt;br&gt;logistic, Logistic Charge&lt;br&gt;other, Other Charge&lt;br&gt;packaging, Packaging Charge&lt;br&gt;pickup, Pickup Charge&lt;br&gt;handling, Handling Charge&lt;br&gt;vat, VAT Charge&lt;br&gt;insurance, Insurance Cost | [optional]
**place_of_incoterm** | **string** | Name of port of departure, shipment or destination as required under the applicable delivery term. | [optional]
**recipient_reference** | **string** | Please enter recipient reference | [optional]
**exporter** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationExporter**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationExporter.md) |  | [optional]
**export_reason_type** | **string** | Please provide the reason for export | [optional]
**shipment_type** | **string** | Please provide the shipment was sent for Personal (Gift) or Commercial (Sale) reasons | [optional]
**customs_documents** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressExportDeclarationCustomsDocuments1[]**](SupermodelIoLogisticsExpressExportDeclarationCustomsDocuments1.md) | Please provide the Customs Documents at invoice level | [optional]
**incoterm** | **string** | The Incoterms rules are a globally-recognized set of standards, used worldwide in international and domestic contracts for the delivery of goods, illustrating responsibilities between buyer and seller for costs and risk, as well as cargo insurance.&lt;br&gt;EXW ExWorks&lt;br&gt;FCA Free Carrier&lt;br&gt;CPT Carriage Paid To&lt;br&gt;CIP Carriage and Insurance Paid To&lt;br&gt;DPU Delivered at Place Unloaded &lt;br&gt;DAP Delivered at Place&lt;br&gt;DDP Delivered Duty Paid&lt;br&gt;FAS Free Alongside Ship&lt;br&gt; FOB Free on Board&lt;br&gt;CFR Cost and Freight&lt;br&gt;CIF Cost,Insurance and Freight&lt;br&gt;&lt;br&gt;&lt;br&gt; |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
