# # SupermodelIoLogisticsExpressExportDeclarationInvoice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **string** | Please enter commercial invoice number |
**date** | **string** | Please enter commercial invoice date |
**function** | **string** | Please provide the purpose was the document details captured and are planned to be used. |
**customer_references** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressExportDeclarationInvoiceCustomerReferences[]**](SupermodelIoLogisticsExpressExportDeclarationInvoiceCustomerReferences.md) | Please provide the customer references at invoice level. Note: customerReference/0/value with typeCode &#39;CU&#39; is mandatory if using POST method and no shipmentTrackingNumber is provided in request. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
