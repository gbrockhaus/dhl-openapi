# # SupermodelIoLogisticsExpressExportDeclarationLineItems

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **int** | Please provide line item number |
**description** | **string** | Please provide description of the line item |
**price** | **float** | Please provide monetary value of the line item |
**quantity** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressExportDeclarationQuantity**](SupermodelIoLogisticsExpressExportDeclarationQuantity.md) |  |
**commodity_codes** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCommodityCodes[]**](SupermodelIoLogisticsExpressCreateShipmentRequestContentExportDeclarationCommodityCodes.md) | Please provide Commodity codes for the shipment at item line level | [optional]
**export_reason_type** | **string** | Please provide the reason for export | [optional]
**manufacturer_country** | **string** | Please enter two letter ISO manufacturer country code |
**weight** | [**AnyOfObjectObject**](AnyOfObjectObject.md) | Please enter the weight information for line item. Either a netValue or grossValue must be provided for the line item. |
**is_taxes_paid** | **bool** | Please provide if the Taxes is paid for the line item | [optional]
**customer_references** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressExportDeclarationCustomerReferences[]**](SupermodelIoLogisticsExpressExportDeclarationCustomerReferences.md) | Please provide the Customer References for the line item | [optional]
**customs_documents** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressExportDeclarationCustomsDocuments[]**](SupermodelIoLogisticsExpressExportDeclarationCustomsDocuments.md) | Please provide the customs documents details | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
