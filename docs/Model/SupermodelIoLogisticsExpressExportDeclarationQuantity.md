# # SupermodelIoLogisticsExpressExportDeclarationQuantity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **int** | Please enter number of pieces in the line item |
**unit_of_measurement** | **string** | Please provide correct unit of measurement&lt;br&gt;&lt;br&gt;Possible values;&lt;br&gt;BOX Boxes&lt;br&gt;2GM Centigram&lt;br&gt;2M Centimeters&lt;br&gt;2M3 Cubic Centimeters&lt;br&gt;3M3 Cubic Feet&lt;br&gt;M3 Cubic Meters&lt;br&gt;DPR Dozen Pairs&lt;br&gt;DOZ Dozen&lt;br&gt;2NO Each&lt;br&gt;PCS Pieces&lt;br&gt;GM Grams&lt;br&gt;GRS Gross&lt;br&gt;KG Kilograms&lt;br&gt;L Liters&lt;br&gt;M Meters&lt;br&gt;3GM Milligrams&lt;br&gt;3L Milliliters&lt;br&gt;X No Unit Required&lt;br&gt;NO Number&lt;br&gt;2KG Ounces&lt;br&gt;PRS Pairs&lt;br&gt;2L Gallons&lt;br&gt;3KG Pounds&lt;br&gt;CM2 Square Centimeters&lt;br&gt;2M2 Square Feet&lt;br&gt;3M2 Square Inches&lt;br&gt;M2 Square Meters&lt;br&gt;4M2 Square Yards&lt;br&gt;3M Yards |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
