# # SupermodelIoLogisticsExpressIdentifier

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type_code** | **string** | Please provide type of the identifier you want to set value for |
**value** | **string** | Please enter value of your identifier (WB number, PieceID) |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
