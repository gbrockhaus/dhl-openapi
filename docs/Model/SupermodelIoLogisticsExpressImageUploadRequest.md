# # SupermodelIoLogisticsExpressImageUploadRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipment_tracking_number** | **string** | Please provide Shipment Identification number (AWB number) |
**original_planned_shipping_date** | **string** |  |
**accounts** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressAccount[]**](SupermodelIoLogisticsExpressAccount.md) | Please enter all the DHL Express accounts and types to be used for this shipment |
**product_code** | **string** | Please enter DHL Express Global Product code |
**document_images** | **object[]** | This section is to support multiple base64 encoded string with the image of export documentation for Paperless Trade images. When an invalid base64 encoded string is provided, an error message will be returned |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
