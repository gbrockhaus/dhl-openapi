# # SupermodelIoLogisticsExpressLandedCostRequestItems

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **float** | Line item number |
**name** | **string** | Name of the item | [optional]
**description** | **string** | Item full description | [optional]
**manufacturer_country** | **string** | ISO Country code of the goods manufacturer | [optional]
**part_number** | **string** | SKU number | [optional]
**quantity** | **float** | Total quantity of the item(s) to be shipped. |
**quantity_type** | **string** | Please provide quantitiy type. prt - part, box - box | [optional]
**unit_price** | **float** | Product Unit price |
**unit_price_currency_code** | **string** | Currency code of the Unit Price |
**customs_value** | **float** | not used | [optional]
**customs_value_currency_code** | **string** | not used | [optional]
**commodity_code** | **string** | commodityCode is mandatory if estimatedTariffRateType (&#39;derived_rate&#39; or &#39;highest_rate&#39; or &#39;lowest_rate&#39; or &#39;center_rate&#39;) not provided in the request otherwise it is considered as Optional. | [optional]
**weight** | **float** | Weight of the item | [optional]
**weight_unit_of_measurement** | **string** | Unit of measurement | [optional]
**category** | **string** | Category code of the Item. \\ 101 - Coats &amp; Jacket \\ 102 - Blazers \\ 103 - Suits \\ 104 - Ensembles \\ 105 - Trousers \\ 106 - Shirts &amp; Blouses \\ 107 - Dresses \\ 108 - Skirts \\ 109 - Jerseys, Sweatshirts &amp; Pullovers \\ 110 - Sports &amp; Swimwear \\ 111 - Night &amp; Underwear \\ 112 - T-Shirts \\ 113 - Tights &amp; Leggings \\ 114 - Socks  \\ 115 - Baby Clothes \\ 116 - Clothing Accessories \\ 201 - Sneakers \\ 202 - Athletic Footwear \\ 203 - Leather Footwear \\ 204 - Textile &amp; Other Footwear \\ 301 - Spectacle Lenses \\ 302 - Sunglasses \\ 303 - Eyewear Frames \\ 304 - Contact Lenses \\ 401 - Watches \\ 402 - Jewelry \\ 403 - Suitcases &amp; Briefcases \\ 404 - Handbags \\ 405 - Wallets &amp; Little Cases \\ 406 - Bags &amp; Containers \\ 501 - Beer \\ 502 - Spirits \\ 503 - Wine \\ 504 - Cider, Perry &amp; Rice Wine \\ 601 - Bottled Water \\ 602 - Soft Drinks \\ 603 - Juices \\ 604 - Coffee \\ 605 - Tea \\ 606 - Cocoa \\ 701 - Dairy Products &amp; Eggs \\ 702 - Meat \\ 703 - Fish &amp; Seafood \\ 704 - Fruits &amp; Nuts \\ 705 - Vegetables \\ 706 - Bread &amp; Cereal Products \\ 707 - Oils &amp; Fats \\ 708 - Sauces &amp; Spices \\ 709 - Convenience Food \\ 710 - Spreads &amp; Sweeteners \\ 711 - Baby Food \\ 712 - Pet Food \\ 801 - Cigarettes \\ 802 - Smoking Tobacco \\ 803 - Cigars \\ 804 - E-Cigarettes \\ 901 - Household Cleaners \\ 902 - Dishwashing Detergents \\ 903 - Polishes \\ 904 - Room Scents \\ 905 - Insecticides \\ 1001 - Cosmetics \\ 1002 - Skin Care \\ 1003 - Personal Care \\ 1004 - Fragrances \\ 1101 - Toilet Paper \\ 1102 - Paper Tissues \\ 1103 - Household Paper \\ 1104 - Feminine Hygiene \\ 1105 - Baby Diapers \\ 1106 - Incontinence \\ 1202 - TV, Radio &amp; Multimedia \\ 1203 - TV Peripheral Devices \\ 1204 - Telephony \\ 1205 - Computing \\ 1206 - Drones \\ 1301 - Refrigerators \\ 1302 - Freezers \\ 1303 - Dishwashing Machines \\ 1304 - Washing Machines \\ 1305 - Cookers &amp; Oven \\ 1306 - Vacuum Cleaners \\ 1307 - Small Kitchen Appliances \\ 1308 - Hair Clippers \\ 1309 - Irons \\ 1310 - Toasters \\ 1311 - Grills &amp; Roasters \\ 1312 - Hair Dryers \\ 1313 - Coffee Machines \\ 1314 - Microwave Ovens \\ 1315 - Electric Kettles \\ 1401 - Seats &amp; Sofas \\ 1402 - Beds \\ 1403 - Mattresses \\ 1404 - Closets, Nightstands &amp; Dressers \\ 1405 - Lamps &amp; Lighting \\ 1406 - Floor Covering \\ 1407 - Kitchen Furniture \\ 1408 - Plastic &amp; Other Furniture \\ 1501 - Analgesics \\ 1502 - Cold &amp; Cough Remedies \\ 1503 - Digestives &amp; Intestinal Remedies \\ 1504 - Skin Treatment \\ 1505 - Vitamins &amp; Minerals \\ 1506 - Hand Sanitizer  \\ 1601 - Toys &amp; Games \\ 1602 - Musical Instruments \\ 1603 - Sports Equipment | [optional]
**brand** | **string** | Item&#39;s brand | [optional]
**goods_characteristics** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressLandedCostRequestGoodsCharacteristics[]**](SupermodelIoLogisticsExpressLandedCostRequestGoodsCharacteristics.md) |  | [optional]
**additional_quantity_definitions** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressLandedCostRequestAdditionalQuantityDefinitions[]**](SupermodelIoLogisticsExpressLandedCostRequestAdditionalQuantityDefinitions.md) |  | [optional]
**estimated_tariff_rate_type** | **string** | &#39;Please enter Tariff Rate Type - default_rate,derived_rate,highest_rate,center_rate,lowest_rate&#39; | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
