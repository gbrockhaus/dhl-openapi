# # SupermodelIoLogisticsExpressPackageReference

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **string** | Please provide reference |
**type_code** | **string** | Please provide reference type&lt;br&gt;&lt;br&gt;AAO, shipment reference number of receiver&lt;br&gt;CU, reference number of consignor - default&lt;br&gt;FF, reference number of freight forwarder&lt;br&gt;FN, freight bill number for &lt;ex works invoice number&gt;&lt;br&gt;IBC, inbound center reference number&lt;br&gt;LLR, load list reference for &lt;10-digit Shipment ID&gt;&lt;br&gt;OBC, outbound center reference number for &lt;SHIPMEN IDENTIFIER (COUNTRY OF ORIGIN)&gt;&lt;br&gt;PRN, pickup request number for &lt;BOOKINGREFERENCE NUMBER&gt;&lt;br&gt;ACP, local payer account number&lt;br&gt;ACS, local shipper account number&lt;br&gt;ACR, local receiver account number&lt;br&gt;CDN, customs declaration number&lt;br&gt;STD, eurolog 15-digit shipment id&lt;br&gt;CO, buyers order number&lt;br&gt;LID, labelless id | [optional] [default to 'CU']

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
