# # SupermodelIoLogisticsExpressPickupRequestCustomerDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipper_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressPickupRequestCustomerDetailsShipperDetails**](SupermodelIoLogisticsExpressPickupRequestCustomerDetailsShipperDetails.md) |  |
**receiver_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressPickupRequestCustomerDetailsShipperDetails**](SupermodelIoLogisticsExpressPickupRequestCustomerDetailsShipperDetails.md) |  | [optional]
**booking_requestor_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressPickupRequestCustomerDetailsBookingRequestorDetails**](SupermodelIoLogisticsExpressPickupRequestCustomerDetailsBookingRequestorDetails.md) |  | [optional]
**pickup_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressPickupRequestCustomerDetailsShipperDetails**](SupermodelIoLogisticsExpressPickupRequestCustomerDetailsShipperDetails.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
