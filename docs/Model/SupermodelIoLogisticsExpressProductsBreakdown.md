# # SupermodelIoLogisticsExpressProductsBreakdown

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**service_code** | **string** | Special service or extra charge code &#39; this is the code you would have to use in the /shipment service if you wish to add an optional Service such as Saturday delivery | [optional]
**local_service_code** | **string** | Local service code | [optional]
**type_code** | **string** |  | [optional]
**service_type_code** | **string** | Special service charge code type for service. | [optional]
**is_customer_agreement** | **bool** | Customer agreement indicator for product and services, if service is offered with prior customer agreement | [optional]
**is_marketed_service** | **bool** | Indicator if the special service is marketed service | [optional]
**is_billing_service_indicator** | **bool** | Indicator if there is any discount allowed | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
