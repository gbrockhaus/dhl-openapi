# # SupermodelIoLogisticsExpressRates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRatesProducts[]**](SupermodelIoLogisticsExpressRatesProducts.md) |  |
**exchange_rates** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRatesExchangeRates[]**](SupermodelIoLogisticsExpressRatesExchangeRates.md) |  | [optional]
**warnings** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
