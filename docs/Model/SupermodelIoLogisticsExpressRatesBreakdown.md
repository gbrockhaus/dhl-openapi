# # SupermodelIoLogisticsExpressRatesBreakdown

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | When landed-cost is requested then following items name (Charge Types) might be returned: \\ Charge Type - Description \\ STDIS - Quoted shipment total discount \\ SCUSV - Shipment Customs value \\ SINSV - Insured value \\ SPRQD - Shipment product quote discount \\ SPRQN - The price quoted to the Customer by DHL at the time of the booking. This quote covers the weight price including discounts and without taxes. \\ STSCH - The total of service charges quoted to customer for DHL Express value added services, the amount is after discounts and doesn&#39;t include tax amounts. \\ MACHG - The total of service charges as provided by Merchant for the purpose of landed cost calculation. \\ MFCHG - The freight charge as provided by Merchant for the purpose of landed cost calculation. | [optional]
**service_code** | **string** | Special service or extra charge code &#39; this is the code you would have to use in the /shipment service if you wish to add an optional Service such as Saturday delivery | [optional]
**local_service_code** | **string** | Local service code | [optional]
**type_code** | **string** |  | [optional]
**service_type_code** | **string** | Special service charge code type for service. | [optional]
**price** | **float** |  | [optional]
**price_currency** | **string** | This the currency of the rated shipment for the prices listed. | [optional]
**is_customer_agreement** | **bool** | Customer agreement indicator for product and services, if service is offered with prior customer agreement | [optional]
**is_marketed_service** | **bool** | Indicator if the special service is marketed service | [optional]
**is_billing_service_indicator** | **bool** | Indicator if there is any discount allowed | [optional]
**price_breakdown** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRatesPriceBreakdown1[]**](SupermodelIoLogisticsExpressRatesPriceBreakdown1.md) |  | [optional]
**tariff_rate_formula** | **string** | Tariff Rate Formula on Shipment Level | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
