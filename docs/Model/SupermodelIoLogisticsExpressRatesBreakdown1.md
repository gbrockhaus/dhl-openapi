# # SupermodelIoLogisticsExpressRatesBreakdown1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Name of the charge | [optional]
**service_code** | **string** | Special service or extra charge code &#39; this is the code you would have to use in the /shipment service if you wish to add an optional Service such as Saturday delivery | [optional]
**local_service_code** | **string** | Local service code | [optional]
**type_code** | **string** | Charge type or category.&lt;br&gt;Possible values;&lt;br&gt;- DUTY&lt;br&gt;- TAX&lt;br&gt;- FEE |
**service_type_code** | **string** | Special service charge code type for service. XCH type charge codes are Optional Services and should be displayed to users for selection.&lt;br&gt;The possible values are;&lt;br&gt;- XCH &#x3D; Extra charge&lt;br&gt;- FEE &#x3D; Fee&lt;br&gt;- SCH &#x3D; Surcharge&lt;br&gt;- NRI &#x3D; Non Revenue Item&lt;br&gt;Other charges may be automatically returned when applicable. | [optional]
**price** | **float** | The charge amount of the line item charge. |
**price_currency** | **string** | This the currency of the rated shipment for the prices listed. | [optional]
**is_customer_agreement** | **bool** | Customer agreement indicator for product and services, if service is offered with prior customer agreement | [optional]
**is_marketed_service** | **bool** | Indicator if the special service is marketed service | [optional]
**is_billing_service_indicator** | **bool** | Indicator if there is any discount allowed | [optional]
**price_breakdown** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRatesPriceBreakdown2[]**](SupermodelIoLogisticsExpressRatesPriceBreakdown2.md) |  | [optional]
**tariff_rate_formula** | **string** | Tariff Rate Formula on Line Item Level | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
