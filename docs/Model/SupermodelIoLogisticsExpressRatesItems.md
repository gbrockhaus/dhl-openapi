# # SupermodelIoLogisticsExpressRatesItems

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **float** | Item line number |
**breakdown** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRatesBreakdown1[]**](SupermodelIoLogisticsExpressRatesBreakdown1.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
