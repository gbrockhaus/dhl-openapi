# # SupermodelIoLogisticsExpressRatesPriceBreakdown

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type_code** | **string** | Expected values in Breakdown/Type are below:&lt;br&gt;STTXA &#39; Total tax for the shipment&lt;br&gt;STDIS &#39; Total discount for the shipment&lt;br&gt;SPRQT &#39; Net shipment / weight charge |
**price** | **float** | The amount price of DHL product and services |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
