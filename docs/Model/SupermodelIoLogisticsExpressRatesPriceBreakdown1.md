# # SupermodelIoLogisticsExpressRatesPriceBreakdown1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_type** | **string** | If a breakdown is provided, details can either be; - \&quot;TAX\&quot;,&lt;br&gt;- \&quot;DISCOUNT\&quot; | [optional]
**type_code** | **string** | Discount or tax type codes as provided by DHL Express. Example values;&lt;br&gt;For discount;&lt;br&gt;P &#39; promotional&lt;br&gt;S &#39; special&lt;br&gt;- \&quot;DISCOUNT\&quot; | [optional]
**price** | **float** | The actual amount of the discount/tax | [optional]
**rate** | **float** | Percentage of the discount/tax | [optional]
**base_price** | **float** | The base amount of the service charge | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
