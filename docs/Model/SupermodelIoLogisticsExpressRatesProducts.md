# # SupermodelIoLogisticsExpressRatesProducts

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_name** | **string** | Name of the DHL Express product | [optional]
**product_code** | **string** | This is the global DHL Express product code for which the delivery is feasible respecting the input data from the request. | [optional]
**local_product_code** | **string** | This is the local DHL Express product code for which the delivery is feasible respecting the input data from the request. | [optional]
**local_product_country_code** | **string** | The country code for the local service used | [optional]
**network_type_code** | **string** | The NetworkTypeCode element indicates the product belongs to the Day Definite (DD) or Time Definite (TD) network.&lt;br&gt;Possible Values;&lt;br&gt;- &#39;DD&#39;, Day Definite product&lt;br&gt;- &#39;TD&#39;, Time Definite product | [optional]
**is_customer_agreement** | **bool** | Indicator that the product only can be offered to customers with prior agreement. | [optional]
**weight** | [**\OpenAPI\Client\Model\Weight1**](Weight1.md) |  |
**total_price** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRatesTotalPrice[]**](SupermodelIoLogisticsExpressRatesTotalPrice.md) |  |
**total_price_breakdown** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRatesTotalPriceBreakdown[]**](SupermodelIoLogisticsExpressRatesTotalPriceBreakdown.md) |  | [optional]
**detailed_price_breakdown** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRatesDetailedPriceBreakdown[]**](SupermodelIoLogisticsExpressRatesDetailedPriceBreakdown.md) |  | [optional]
**pickup_capabilities** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRatesPickupCapabilities**](SupermodelIoLogisticsExpressRatesPickupCapabilities.md) |  | [optional]
**delivery_capabilities** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressProductsDeliveryCapabilities**](SupermodelIoLogisticsExpressProductsDeliveryCapabilities.md) |  | [optional]
**items** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRatesItems[]**](SupermodelIoLogisticsExpressRatesItems.md) |  | [optional]
**pricing_date** | **string** | The date when the rates for DHL products and services is provided | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
