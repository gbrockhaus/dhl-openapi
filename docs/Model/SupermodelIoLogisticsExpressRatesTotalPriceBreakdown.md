# # SupermodelIoLogisticsExpressRatesTotalPriceBreakdown

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency_type** | **string** | Possible Values :&lt;br&gt;- &#39;BILLC&#39;, billing currency&lt;br&gt;- &#39;PULCL&#39;, country public rates currency&lt;br&gt;- &#39;BASEC&#39;, base currency | [optional]
**price_currency** | **string** | This the currency of the rated shipment for the prices listed. | [optional]
**price_breakdown** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRatesPriceBreakdown[]**](SupermodelIoLogisticsExpressRatesPriceBreakdown.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
