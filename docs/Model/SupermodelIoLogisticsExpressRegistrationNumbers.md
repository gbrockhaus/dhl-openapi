# # SupermodelIoLogisticsExpressRegistrationNumbers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type_code** | **string** | VAT, Value-Added tax&lt;br&gt;EIN, Employer Identification Number&lt;br&gt;GST, Goods and Service Tax&lt;br&gt;SSN, Social Security Number&lt;br&gt;EOR, European Union Registration and Identification&lt;br&gt;DUN, Data Universal Numbering System&lt;br&gt;FED, Federal Tax ID&lt;br&gt;STA, State Tax ID&lt;br&gt;CNP, Brazil CNPJ/CPF Federal Tax&lt;br&gt;IE, Brazil type IE/RG Federal Tax&lt;br&gt;INN, Russia bank details section - INN&lt;br&gt;KPP, Russia bank details section - KPP&lt;br&gt;OGR, Russia bank details section - OGRN&lt;br&gt;OKP, Russia bank details section - OKPO&lt;br&gt;SDT, Overseas Registered Supplier or AUSid GST Registration or VAT on E-Commerce&lt;br&gt;FTZ, Free Trade Zone ID&lt;br&gt;DAN, Deferment Account Duties Only&lt;br&gt;TAN, Deferment Account Tax Only&lt;br&gt;DTF, Deferment Account Duties, Taxes and Fees Only&lt;br&gt;RGP, EU Registered Exporters registration ID&lt;br&gt; DLI, Driver&#39;s License &lt;br&gt;NID, National Identity Card&lt;br&gt;PAS, Passport&lt;br&gt;MID, Manufacturer ID | [default to 'VAT']
**number** | **string** | Please enter registration number |
**issuer_country_code** | **string** | Please enter 2 character code of the country where the Registration Number has been issued by |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
