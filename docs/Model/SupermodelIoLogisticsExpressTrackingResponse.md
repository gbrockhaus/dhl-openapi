# # SupermodelIoLogisticsExpressTrackingResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipments** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressTrackingResponseShipments[]**](SupermodelIoLogisticsExpressTrackingResponseShipments.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
