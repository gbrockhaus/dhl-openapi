# # SupermodelIoLogisticsExpressTrackingResponseEvents1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **string** |  | [optional]
**time** | **string** |  | [optional]
**type_code** | **string** |  | [optional]
**description** | **string** |  | [optional]
**service_area** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressTrackingResponseServiceArea1[]**](SupermodelIoLogisticsExpressTrackingResponseServiceArea1.md) |  | [optional]
**signed_by** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
