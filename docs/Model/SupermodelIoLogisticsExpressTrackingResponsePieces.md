# # SupermodelIoLogisticsExpressTrackingResponsePieces

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **float** |  | [optional]
**type_code** | **string** |  | [optional]
**shipment_tracking_number** | **string** |  | [optional]
**tracking_number** | **string** |  | [optional]
**description** | **string** |  | [optional]
**weight** | **float** | The weight of the package. | [optional]
**dimensional_weight** | **float** | The weight of the package. | [optional]
**actual_weight** | **float** | The weight of the package. | [optional]
**dimensions** | [**\OpenAPI\Client\Model\Dimensions2**](Dimensions2.md) |  | [optional]
**actual_dimensions** | [**\OpenAPI\Client\Model\Dimensions**](Dimensions.md) |  | [optional]
**unit_of_measurements** | **string** |  | [optional]
**shipper_references** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressReference[]**](SupermodelIoLogisticsExpressReference.md) |  | [optional]
**events** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressTrackingResponseEvents1[]**](SupermodelIoLogisticsExpressTrackingResponseEvents1.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
