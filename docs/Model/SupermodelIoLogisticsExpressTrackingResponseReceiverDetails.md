# # SupermodelIoLogisticsExpressTrackingResponseReceiverDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**postal_address** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressTrackingResponseReceiverDetailsPostalAddress**](SupermodelIoLogisticsExpressTrackingResponseReceiverDetailsPostalAddress.md) |  | [optional]
**service_area** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressTrackingResponseReceiverDetailsServiceArea[]**](SupermodelIoLogisticsExpressTrackingResponseReceiverDetailsServiceArea.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
