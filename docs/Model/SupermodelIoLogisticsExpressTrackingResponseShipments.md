# # SupermodelIoLogisticsExpressTrackingResponseShipments

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipment_tracking_number** | **string** |  | [optional]
**status** | **string** |  | [optional]
**shipment_timestamp** | **string** |  | [optional]
**product_code** | **string** | DHL product code | [optional]
**description** | **string** |  | [optional]
**shipper_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressTrackingResponseShipperDetails**](SupermodelIoLogisticsExpressTrackingResponseShipperDetails.md) |  | [optional]
**receiver_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressTrackingResponseReceiverDetails**](SupermodelIoLogisticsExpressTrackingResponseReceiverDetails.md) |  | [optional]
**total_weight** | **float** |  | [optional]
**unit_of_measurements** | **string** |  | [optional]
**shipper_references** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressReference[]**](SupermodelIoLogisticsExpressReference.md) |  | [optional]
**events** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressTrackingResponseEvents[]**](SupermodelIoLogisticsExpressTrackingResponseEvents.md) |  |
**number_of_pieces** | **float** |  | [optional]
**pieces** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressTrackingResponsePieces[]**](SupermodelIoLogisticsExpressTrackingResponsePieces.md) |  | [optional]
**estimated_delivery_date** | **string** |  | [optional]
**children_shipment_identification_numbers** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
