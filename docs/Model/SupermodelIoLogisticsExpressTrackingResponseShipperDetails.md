# # SupermodelIoLogisticsExpressTrackingResponseShipperDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**postal_address** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressTrackingResponseShipperDetailsPostalAddress**](SupermodelIoLogisticsExpressTrackingResponseShipperDetailsPostalAddress.md) |  | [optional]
**service_area** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressTrackingResponseShipperDetailsServiceArea[]**](SupermodelIoLogisticsExpressTrackingResponseShipperDetailsServiceArea.md) |  | [optional]
**account_number** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
