# # SupermodelIoLogisticsExpressTrackingResponseShipperDetailsPostalAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city_name** | **string** |  | [optional]
**county_name** | **string** |  | [optional]
**postal_code** | **string** |  | [optional]
**province_code** | **string** | The region in which the locality is, and which is in the country. | [optional]
**country_code** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
