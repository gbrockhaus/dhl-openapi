# # SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seller_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsSellerDetails**](SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsSellerDetails.md) |  | [optional]
**buyer_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsBuyerDetails**](SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsBuyerDetails.md) |  | [optional]
**importer_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsImporterDetails**](SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsImporterDetails.md) |  | [optional]
**exporter_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsExporterDetails**](SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsExporterDetails.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
